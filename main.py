import argparse
from bs4 import BeautifulSoup
import os
import wget


def main(file):
    with open(file, "r", encoding="utf-8") as SF:
        vocabulary = SF.read().split('<div class="vocab-list-row vocab-list-row--keyphrase">')

    parsed_words = []

    try:
        os.chdir("images")
    except OSError:
        os.mkdir("images")
        os.chdir("images")

    count = 1
    for raw_word in vocabulary[1:]:
        print(str(round(100 / len(vocabulary[1:]) * count, 2)) + "% | " + str(count) + "/" + str(len(vocabulary[1:])))
        soup = BeautifulSoup(raw_word, "html.parser")

        raw_target = soup.find("div", class_="vocab-list-row__course-language")
        raw_native = soup.find("div", class_="vocab-list-row__interface-language")
        raw_target_sentence = soup.find("div", class_="vocab-list-row__keyphrase-course")
        raw_native_sentence = soup.find("div", class_="vocab-list-row__keyphrase-interface")

        image_name = soup.find("img", class_="vocab-list-row__image")['src']
        image = '<img src="' + image_name.split('/')[-1] + '">'
        wget.download(image_name)

        parsed_word = {'target_language': raw_target.get_text()[:-1].replace('\n', ''),
                       'native_language': raw_native.get_text().replace('\n', ''),
                       'target_sentence': raw_target_sentence.get_text().replace('\n', ''),
                       'native_sentence': raw_native_sentence.get_text().replace('\n', ''),
                       'image_name': image}

        parsed_words.append(parsed_word)
        count += 1

    os.chdir('..')
    new_file = open("anki_export.txt", "w")
    for word in parsed_words:
        new_file.write(
            word['target_language'] + ";" + word['native_language'] + ';' + word['target_sentence'] + ';' + word[
                'native_sentence'] + ';' + word['image_name'] + '\n')

    new_file.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Przygotuj plik z kodem źródłowym ze strony słówek na Busuu")
    parser.add_argument("-f", "--file", help="Plik z kodem, html lub txt", required=True)
    args = parser.parse_args()
    main(args.file)
